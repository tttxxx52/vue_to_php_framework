/* eslint-disable semi */
import crypto from 'crypto'
const algorithm = 'aes-256-cfb';

export default {
  encryptText (text) {
    return  crypto.createHash('sha256').update(text).digest('base64')
  },
}
