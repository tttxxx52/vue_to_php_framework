import crypto from "../util/crypto"
import auth from "../util/auth"
import Vue from 'vue'
import router from '../router'

let url = '';

export default {
    fetchWithEncrypt(){ /* 整個參數json 轉成 string 再進行加密 */
        url = '';
        const args = Array.prototype.slice.call(arguments);
        const action = args[0][0];
        const paramter = crypto.encryptText(JSON.stringify(args[1]));
        const funcSuccess = args[2];
        this.send( paramter, funcSuccess);
    },
    fetch(){
        url = '';
        const args = Array.prototype.slice.call(arguments);
        let value = args[0][0];
        value =  value.replace("(", "");
        value =  value.replace(")", "");
        let commaNum = value.indexOf(",");
        let module  = value.slice(0,commaNum).trim();
        let action  = value.slice(commaNum+1).trim();
        url = "module="+module+"&action="+action;

        const paramter = JSON.stringify(args[1]);
        const funcSuccess = args[2];
        this.send( paramter, funcSuccess);
    },
    async send( paramter, funcSuccess){
       await fetch(process.env.VUE_APP_API_HOST + url,{
            body: JSON.stringify({
                'parameters': paramter,
            }),
            headers: new Headers({
                'Content-Type': 'application/json',
            }),
            method: 'POST'
        }).then(response => {
            return response.json();
        }).then(json => {
            if (json.status_code === -100){ //尚未登入無權限
                this.$router.push({name: 'Login'});
            }else {
                funcSuccess(json);
            }
        }).catch(error => {
            console.log(error)
           Vue.prototype.$message({
               type: 'error',
               message: "error",
               duration: 2000,
           })
        });
    }
}
