import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'

import Login from './views/Login.vue'

//首頁/系統概覽
import Welcome from './views/member/Welcome.vue'
import MainPage from './views/MainPage.vue'


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: '',
  routes: [
      //會員管理-登入紀錄
      {
          path: '/',
          name: 'Login',
          component: Login
      },
      {
          path: '/admin',
          name: 'mainPage',
          component: MainPage,
          children: [
              {path: 'welcome', name: 'Welcome', component: Welcome, meta: {group:'系统概览',title: ''}},
          ]
      },
  ]
})
