import Vue from 'vue'
import App from './App.vue'
import router from './router'

// ===================================== 外部套件 =====================================

//---- Bootstrap
import Bootstrap from 'bootstrap'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//---- MainCSS
import '../src/assets/theme/css/style_admin.css'

//---- jQuery
// import {$, jQuery} from 'jquery'

//---- ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI);

//---- Crypto
import Crypto from './util/crypto'
import {jQuery} from 'jquery'

window.$ = jQuery;
window.JQuery = jQuery;

Crypto.install = function (Vue) {
    Vue.prototype.$crypto = Crypto;
};
Vue.use(Crypto);

//---- Auth
import Auth from './util/auth'

Auth.install = function (Vue) {
    Vue.prototype.$auth = Auth;
};
Vue.use(Auth);

//---- Http
import Http from './util/http'

//---- Clipboard
import Clipboard from 'v-clipboard';
Vue.use(Clipboard);

Http.install = function (Vue) {
    Vue.prototype.$http = Http;
};
Vue.use(Http);


// Vue.config.productionTip = false;
console.log("=> web_console version: 2019-05-07 17:06:10 <=");
new Vue({
    Bootstrap,
    router,
    render: h => h(App)
}).$mount('#app');
