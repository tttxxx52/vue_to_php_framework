const prod = process.env.NODE_ENV === "production";
const webpack = require('webpack');

module.exports = {
    publicPath: prod ? "/admin" : "/",
    productionSourceMap:  !prod,
    chainWebpack: config => {
        config
            .plugin('provide')
    .use(webpack.ProvidePlugin, [{
            $: 'jquery',
        jquery: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    }]);
    },
};
